package isel.poo;

import isel.poo.fs.*;
import static isel.poo.fs.Entry.*;

public class TestFS {
    public static void main(String[] args) {
        Entry file = createFile("aula.txt", 100);
        Dir disk = createDir("C:");
        Dir dir = createDir("POO");

        disk.add(createFile("file.txt", 1024));
        disk.add(dir);
        disk.add(createDir("Empty"));
        dir.add(file);
        dir.add(createFile("Str.java", 2340));

        // C:{file.txt;POO{aula.txt;Str.java};Empty{}}
        System.out.println( Visitor.toTree(disk) );
        System.out.println("POO size="+Visitor.toSize(dir)); // POO size = 2440
        System.out.println("C: size="+Visitor.toSize(disk)); // C: size = 3464
        System.out.println("path = "+Visitor.toPath(file)); // path = C:/POO/aula.txt
    }
}

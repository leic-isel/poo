package isel.poo.fs;

public class Visitor {
    public static String toTree(Entry e) {
        StringBuilder res = new StringBuilder(e.name);
        if (e.isDir()) {
            res.append('{');
            boolean first = true;
            for (Entry f : e) {
                if (!first) res.append(';');
                res.append(toTree(f));
                first = false;
            }
            res.append('}');
        }
        return res.toString();
    }

    public static int toSize(Entry e) {
        int size = 0;
        if (e.isDir()) {
            for(Entry f : e) size += toSize(f);
            /*
            Entry f = e.getFirst();
            do size += toSize( f );
            while ( (f=e.getNext()) != null);
             */
        } else
            size = e.size;
        return size;
    }

    public static String toPath(Entry e) { // path = C:/POO/aula.txt
        Entry p = e.getParent();
        if (p == null) return e.name;
        return toPath(p) + "/" + e.name;
    }
}

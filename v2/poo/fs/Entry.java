package isel.poo.fs;

import java.util.Iterator;

public class Entry implements Iterable<Entry> {
    private static final int MAX_ENTRIES = 100;
    public final String name;
    public final int size;  // -1 ==> DIR
    private Entry[] entries;
    private int numOfEntries=0;

    private Entry parent;

    private Entry(String fileName, int fileSize) {
        name = fileName;
        size = fileSize;
    }

    public static Entry createFile(String fileName, int fileSize) {
        return new Entry(fileName,fileSize);
    }
    public static Entry createDir(String dirName) {
        Entry dir = new Entry(dirName,-1);
        dir.entries = new Entry[MAX_ENTRIES];
        return dir;
    }

    public void add(Entry entry) {
        entries[numOfEntries++]=entry;
        entry.parent = this;
    }

    public Entry getParent() { return parent; }

    public Entry[] getEntries() {return  entries;}

    /*
    private int idx = 0;
    public Entry getFirst() {
        idx=0; return getNext();
    }
    public Entry getNext() {
        return idx<numOfEntries ? entries[idx++] : null;
    }
   */

    public boolean isDir() { return size < 0; }

    @Override
    public Iterator<Entry> iterator() {
        return new Iterator<>() {
            private int idx = 0;
            @Override
            public boolean hasNext() { return idx<numOfEntries; }
            @Override
            public Entry next() { return entries[idx++]; }
        };
    }
}

package isel.poo;

public class Str extends Object {
    private static final int INIT_SIZE = 32;
    private char[] txt;
    private int len;

    public Str(String s) {
        int size = INIT_SIZE;
        len = s.length();
        while (size < len) size *= 2;
        txt = new char[size];

        for (int i = 0; i < len; i++)
            txt[i] = s.charAt(i);
    }

    @Override
    public String toString() {
        return new String(txt,0,len);
    }

    public char charAt(int idx) {
        return txt[idx];
    }

    public void setCharAt(char c, int idx) {
        txt[idx] = c;
    }

    public void append(String s) {
        //TODO: Implementar para o novo comportamento.
        /*  IMPLEMENTAÇÃO ANTIGA
        char[] ntxt = new char[ txt.length + s.length()];
        int i;
        for (i = 0; i < txt.length; i++) ntxt[i]=txt[i];
        for ( ; i <ntxt.length ; i++) ntxt[i]=s.charAt(i-txt.length);
        txt = ntxt;
        */
        int nlen = len + s.length();  // new length, original String len + s.length()

        int size = INIT_SIZE;
        while (nlen > txt.length) size *= 2;  // if nlen is more than txt.length, double the size

        char[] ntxt = new char[size];
        int i;
        for (i = 0; i < len; i++) ntxt[i] = txt[i];
        for (; i < nlen; i++) ntxt[i] = s.charAt(i - len);
        len = nlen;  // update len
        txt = ntxt;  // update txt
    }

    public int length() {
        return len;
    }
}

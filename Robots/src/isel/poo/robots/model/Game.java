package isel.poo.robots.model;

import java.util.Iterator;

public class Game implements Iterable<Actor> {
    private Person hero;
    Arena arena;
    private int level = 1;

    public Game(int lines, int cols) {
        Position.init(lines,cols);
        arena = new Arena(lines,cols);
        hero = new Person(lines/2,cols/2,this);
        initEnemies();
    }

    private void initEnemies() {
        int i = 0;
        int numEnemies = 10 * level;
        while (i < numEnemies) {
            Position pos = Position.getRandomPosition();
            if (!arena.hasActor(pos)){
                Enemy enemy = new Enemy(pos.line, pos.col, this);
                arena.set(enemy);
                i++;
            }
        }
    }

    public int getLines() { return Position.getLines(); }
    public int getCols() { return Position.getCols();  }

    public void moveHero(Dir dir) { hero.move(dir); }

    public void moveEnemies() {
        for (Actor actor: arena) {
            if (actor instanceof Enemy) {
                Dir newDir = calcDir(hero.position, actor.position);
                actor.move(newDir);
            }
        }
    }

    private Dir calcDir(Position heroPos, Position enemyPos) {
        Dir dir = null;
        if (heroPos.line > enemyPos.line && heroPos.col == enemyPos.col) dir = Dir.DOWN;
        else if (heroPos.line > enemyPos.line && heroPos.col > enemyPos.col) dir = Dir.DOWN_RIGHT;
        else if (heroPos.line > enemyPos.line && heroPos.col < enemyPos.col) dir = Dir.DOWN_LEFT;
        else if (heroPos.line < enemyPos.line && heroPos.col == enemyPos.col) dir = Dir.UP;
        else if (heroPos.line < enemyPos.line && heroPos.col > enemyPos.col) dir = Dir.UP_RIGHT;
        else if (heroPos.line < enemyPos.line && heroPos.col < enemyPos.col) dir = Dir.UP_LEFT;
        else if (heroPos.line == enemyPos.line && heroPos.col > enemyPos.col) dir = Dir.RIGHT;
        else if (heroPos.line == enemyPos.line && heroPos.col < enemyPos.col) dir = Dir.LEFT;

        return dir;
    }

    @Override
    public Iterator<Actor> iterator() { return arena.iterator(); }
}

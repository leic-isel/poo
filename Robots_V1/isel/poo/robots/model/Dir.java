package isel.poo.robots.model;

public class Dir {
    public final int dl;
    public final int dc;

    public static Dir UP = new Dir(-1,0);
    public static Dir DOWN = new Dir(+1,0);
    public static Dir LEFT = new Dir(0,-1);
    public static Dir RIGHT = new Dir(0,+1);
    public static Dir UP_LEFT = new Dir(-1,-1);
    public static Dir UP_RIGHT = new Dir(-1,+1);
    public static Dir DOWN_LEFT = new Dir(+1,-1);
    public static Dir DOWN_RIGHT = new Dir(+1,+1);

    private Dir(int dl, int dc) {
        this.dl = dl;
        this.dc = dc;
    }
}

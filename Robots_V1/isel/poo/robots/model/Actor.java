package isel.poo.robots.model;

public class Actor {
    private int line;
    private int col;
    private Game game;

    public Actor(int line, int col, Game game) {
        this.line = line;
        this.col = col;
        this.game = game;
    }

    public void move(Dir dir) {
        int l = line+dir.dl, c = col+dir.dc;
        if (l >=0 && l<game.getLines()) line = l;
        if (c >=0 && c<game.getCols()) col = c;
    }
}

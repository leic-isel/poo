package isel.poo.robots.model;

public class Game {
    final int lines;
    final int cols;
    Person hero;

    public Game(int lines, int cols) {
        this.lines = lines;
        this.cols = cols;
        hero = new Person(lines/2,cols/2,this);
    }
    public int getLines() { return lines; }
    public int getCols() { return cols;  }

    public void moveHero(Dir dir) {
        hero.move(dir);
    }
}

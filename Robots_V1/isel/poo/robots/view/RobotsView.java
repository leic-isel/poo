package isel.poo.robots.view;

import isel.poo.console.Window;
import isel.poo.console.tile.TilePanel;
import isel.poo.robots.model.Game;

public class RobotsView {
    private Game model;
    private Window win;
    private TilePanel panel;

    public RobotsView(Game model) {
        this.model = model;
        int lines = model.getLines(), cols = model.getCols();
        int height = lines+4, width = cols+4;
        win = new Window("Robots",height,width);
        panel = new TilePanel(lines,cols,1);
        panel.center(height,width);
        panel.repaint();
    }

    public void close() {
        win.message("Bye");
        win.close();
    }
}

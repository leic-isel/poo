package isel.poo.robots.model;

import java.util.Iterator;

public class Game implements Iterable<Actor> {
    Person hero;
    Arena arena;

    public Game(int lines, int cols) {
        Position.init(lines,cols);
        arena = new Arena(lines,cols);
        hero = new Person(lines/2,cols/2,this);
    }
    public int getLines() { return Position.getLines(); }
    public int getCols() { return Position.getCols();  }

    public void moveHero(Dir dir) {
        hero.move(dir);
    }

    public Person getHero() { return hero; }

    @Override
    public Iterator<Actor> iterator() { return arena.iterator(); }
}

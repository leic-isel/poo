package isel.poo.robots.view;

import isel.poo.console.Window;
import isel.poo.console.tile.Tile;
import isel.poo.console.tile.TilePanel;
import isel.poo.robots.model.*;

public class RobotsView {
    private Game model;
    private Window win;
    private TilePanel panel;
    private PersonView person = new PersonView();

    public RobotsView(Game model) {
        this.model = model;
        int lines = model.getLines(), cols = model.getCols();
        int height = lines+4, width = cols+4;
        win = new Window("Robots",height,width);
        panel = new TilePanel(lines,cols,1);
        panel.center(height,width);
        panel.repaint();
        repaint();
    }

    public void close() {
        win.message("Bye");
        win.close();
    }

    public void repaint() {
        Position pos = model.getHero().getPosition();
        panel.setTile(pos.line, pos.col, person);
    }

    public void clear() {
        Position pos = model.getHero().getPosition();
        panel.setTile(pos.line, pos.col, new Tile());
    }
}

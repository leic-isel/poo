package isel.poo.robots.view;

import isel.leic.pg.Console;
import isel.poo.console.tile.Tile;

import java.awt.*;

public class PersonView extends Tile {
    @Override
    public void paint() {
        Console.color(Console.WHITE, Console.BLACK);
        print(0,0,"P");
    }
}

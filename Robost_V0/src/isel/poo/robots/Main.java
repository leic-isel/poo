package isel.poo.robots;

import isel.leic.pg.Console;
import isel.poo.robots.model.Dir;
import isel.poo.robots.model.Game;
import isel.poo.robots.view.RobotsView;
import java.awt.event.KeyEvent;

public class Main {
    private static final int LINES=25, COLS=40;
    private Game model = new Game(LINES,COLS);
    private RobotsView view = new RobotsView(model);

    public static void main(String[] args) { new Main().run(); }

    private void run() {
        for(;;) {
            int key = Console.waitKeyPressed(1000);
            if (key==KeyEvent.VK_ESCAPE) break;
            if (key!=Console.NO_KEY) {
                processKey(key);
                Console.waitKeyReleased(key);
            }
        }
        view.close();
    }

    private void processKey(int k) {
        Dir d = null;
        switch (k) {
            case KeyEvent.VK_UP: d = Dir.UP; break;
            case KeyEvent.VK_DOWN: d = Dir.DOWN; break;
            case KeyEvent.VK_LEFT: d = Dir.LEFT; break;
            case KeyEvent.VK_RIGHT: d = Dir.RIGHT; break;
            case KeyEvent.VK_HOME: d = Dir.UP_LEFT; break;
            case KeyEvent.VK_PAGE_UP: d = Dir.UP_RIGHT; break;
            case KeyEvent.VK_END: d = Dir.DOWN_LEFT; break;
            case KeyEvent.VK_PAGE_DOWN: d = Dir.DOWN_RIGHT; break;
        }
        if (d!=null) {
            model.moveHero(d);
            model.repaint();
        }
    }


}

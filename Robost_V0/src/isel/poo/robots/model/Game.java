package isel.poo.robots.model;

public class Game {
    Person hero;

    public Game(int lines, int cols) {
        Position.init(lines, cols);
        hero = new Person(lines/2,cols/2,this);
    }
    public int getLines() { return Position.getLines(); }
    public int getCols() { return Position.getCols();  }

    public void moveHero(Dir dir) {
        hero.move(dir);
    }

    public Person getHero() { return hero; }
}

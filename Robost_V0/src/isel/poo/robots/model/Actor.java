package isel.poo.robots.model;

public class Actor {
    Position position;
    private Game game;

    public Actor(int line, int col, Game game) {
        position = Position.of(line, col);
        this.game = game;
    }

    public void move(Dir dir) {
        position = position.add(dir);
    }

    public Position getPosition() { return position; }
}

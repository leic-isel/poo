package isel.poo.robots.model;

public class Position {
    public final int line;
    public final int col;

    private Position(int l, int c) {
        line = l;
        col = c;
    }

    private static Position[][] all;

    static void init(int lines, int cols) {
        all = new Position[lines][cols];
    }

    public static Position of(int l, int c) {
        Position pos = all[l][c];
        if (pos==null) {
            all[l][c] = pos = new Position(l, c);
        }
        return pos;
    }

    public static  int getLines() {return all.length; }
    public static int getCols() {return  all[0].length;}

    public Position add(Dir dir){
        int l = line+dir.dl, c = col+dir.dc;
        if (l<0 || l>=all.length) l = line;
        if (c<0 || c>=all[0].length) c = col;
        return Position.of(l,c);
    }
}
